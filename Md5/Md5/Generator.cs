﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Md5
{
    internal class Generator
    {
        private static string GetMD5()
        {
            var bytes = new byte[8];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(bytes);
            }
            return BitConverter.ToString(bytes).Replace("-", "").ToLower();
        }
        public static void initFile(string fileName, int count)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.Create)))
            {
                for (var i = 0; i != count; ++i)
                {
                    byte[] arr = System.Text.Encoding.ASCII.GetBytes(GetMD5());
                    writer.Write(arr);
                }

            }
        }
    }
}
