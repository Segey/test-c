﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Md5
{
    internal class Worker
    {
        private readonly List<long> _data = new List<long>();
        private delegate void Del(object o);
        private readonly static Mutex mutex = new Mutex();     

        struct Obj
        {
            public string _filename;
            public long _index;
            public long _size;
            public Obj(string f, long index, long size)
            {
                _filename = f;
                _index = index;
                _size = size;
            }
        }

        private void Done(Int64 value)
        {
            lock(mutex)
            {
                _data.Add(value);
            }
        }

        public void Run(object o)
        {
            Obj obj = (Obj)o;
            using (FileStream readStream = new FileStream(obj._filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (BinaryReader read = new BinaryReader(readStream))
                {
                    read.BaseStream.Position = obj._index * obj._size;
                    while (read.BaseStream.Position < obj._index * obj._size + obj._size)
                    {
                        var s = new string(read.ReadChars(256));
                        for (int i = 0; i < s.Length; i += 16)
                        {
                           Done(Convert.ToInt64(s.Substring(i, 16), 16));
                        }
                    }
                }
            }
        }

        public bool ReadFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                var f = new FileInfo(fileName);
                var size = f.Length/16;

                var threads = new List<Thread>();
                for(var i = 0; i != 16; ++i)
                {
                    threads.Add(new Thread(Run));
                }

                int cx = 0;
                foreach (Thread curThread in threads)
                {
                    curThread.Start(new Obj(fileName, cx++, size));
                }

                foreach (Thread curThread in threads)
                {
                    curThread.Join();
                }
                _data.Sort();
            }
            return true;
        }
        public bool Add(string str)
        {
            _data.Add(Convert.ToInt64(str, 16));
            _data.Sort();
            return true;
        }
          public bool Find(string str)
        {
            return _data.BinarySearch(Convert.ToInt64(str, 16)) >= 0;
        }
        public int Count
        {
            get
            {
                return _data.Count;
            }
        }
    }
}
