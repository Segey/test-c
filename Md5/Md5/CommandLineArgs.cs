﻿using System;
using System.Collections.Generic;

namespace Md5
{
    class CommandLineArgs
    {
        const string fileName = @"data.txt";
        const int count = 10000000;
        const string hash = "33995bf6b5939a35";  // b10ae01428cc77a2

        public void Parse(string[] args)
        {
            if (args.Length < 1 || args[0] == "-h" || args[0] == "--help")
            {
                PrintHelp();
            }
            else if (args[0] == "-c" || args[0] == "--create")
            {                    
                int val = args.Length < 2 ? count : Convert.ToInt32(args[1]);
                CreateFile(val);
            }
            else if (args[0] == "-i" || args[0] == "--init")
            {
                ReadFile();
            }
            else if (args[0] == "-f" || args[0] == "--find")
            {
                string str = args.Length < 2 ? hash : args[1];
                FindHash(str);
            }
        } 
        private void PrintHelp()
        {
            Console.WriteLine("Usage");
            Console.WriteLine("    -h, --help This help");
            Console.WriteLine("    -c, --create Create a file with md5 data (where n number of hashes)");
            Console.WriteLine("    -i, --init Read a file with md5 data");
            Console.WriteLine("    -f, --find Find a hash");
            Console.WriteLine("Examples:");
            Console.WriteLine("  MD5.exe -c 1000");
            Console.WriteLine("  MD5.exe -i");
            Console.WriteLine("  MD5.exe -f 33995bf6b5939a35");
        }
        private void CreateFile(int val)
        {
            Generator.initFile(fileName, val);
            Console.WriteLine($"{fileName} file has been created with {val} hashes!");
        }
        private void ReadFile()
        {
            var w = new Worker();
            w.ReadFile(fileName);
            Console.WriteLine($"{w.Count} unique hashes have been added!");
        }
        private void FindHash(string str)
        {
            var w = new Worker();
            w.ReadFile(fileName);

            var result = w.Find(str);
            if (result)
                Console.WriteLine($"{str} hash found in the {fileName} file!");
            else
                Console.WriteLine($"{str} hash hasn't found in the {fileName} file!");
        }
    }
}